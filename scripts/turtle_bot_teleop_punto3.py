#!/usr/bin/env python3
import sys
from geometry_msgs.msg import Twist
import rospy
import threading
from getkey import getkey, keys
import keyboard
import numpy as np
from pynput import keyboard
import time
import getch
# Universidad de los Andes
# Robotica 2020 - 20
# Taller 1
# Solucion del primer punto Taller 1 Robotica 2020 - 20

class TurtleBotTeleop():

	def __init__(self):
		self.vel_Lineal = 0
		self.vel_Angular = 0
		self.cmdVelPub = None
		self.message = Twist()
		self.current_keys = set()
		self.keys_pressed = []
		self.combination_to_function = {
		frozenset([keyboard.KeyCode(char ='w'), keyboard.KeyCode(char ='d')]): self.action_1,
		frozenset([keyboard.KeyCode(char ='w'), keyboard.KeyCode(char ='a')]): self.action_2,
		frozenset([keyboard.KeyCode(char ='s'), keyboard.KeyCode(char ='d')]): self.action_3,
		frozenset([keyboard.KeyCode(char ='s'), keyboard.KeyCode(char ='a')]): self.action_4
		}
	
	def action_1(self):
		self.vel_Lineal = 2
		self.vel_Angular = -2

	def action_2(self):
		self.vel_Lineal = 2
		self.vel_Angular = 2

	def action_3(self):
		self.vel_Lineal = -2
		self.vel_Angular = -2

	def action_4(self):
		self.vel_Lineal = -2
		self.vel_Angular = 2

	def on_press(self, key):
		self.current_keys.add(key)
		self.keys_pressed.append(key)
		print(self.keys_pressed)
		if frozenset(self.current_keys) in self.combination_to_function:
			self.combination_to_function[frozenset(self.current_keys)]()
		
		elif key.char == 'w':   #left
			self.vel_Lineal = 3
			self.vel_Angular = 0
		elif key.char == 's': #right
			self.vel_Lineal = -3
			self.vel_Angular = 0
		elif key.char == 'a':  #jump
			self.vel_Lineal = 0
			self.vel_Angular = 3
		elif key.char == 'd':
			self.vel_Lineal = 0
			self.vel_Angular = -3
		else:
			self.vel_Lineal = 0
			self.vel_Angular = 0 

	def on_release(self, key): #Funcion al soltar una tecla
		print('{0} release'.format(key))
		self.vel_Lineal = 0
		self.vel_Angular = 0
		if key == keyboard.Key.esc: #Con ESC finaliza este thread
			return False
		try:
			self.current_keys.remove(key)
		except:
			pass

	def ThreadInputs(self): #Listener de Pynput en otro thread
		with keyboard.Listener(on_press=self.on_press, on_release=self.on_release) as listener:
			listener.join()

	def main(self):
		rospy.init_node('turtle_bot_teleop', anonymous = False)
		self.actionPub = rospy.Publisher('/turtlebot_cmdVel', Twist , queue_size = 1)
		respuesta = input('Desea guardar el recorrido? s o n ')
		print(respuesta)
		threading.Thread(target=self.ThreadInputs).start()
		rate = rospy.Rate(100)
		while not rospy.is_shutdown():
			self.message.linear.x = self.vel_Lineal
			self.message.linear.y = 0
			self.message.linear.z = 0
			self.message.angular.x = 0
			self.message.angular.y = 0
			self.message.angular.z = self.vel_Angular
			self.actionPub.publish(self.message)
			if respuesta == 's':
				np.savetxt("keys_pressed.txt",self.keys_pressed, fmt="%s")
			rate.sleep()

if __name__ == '__main__':
	turtleBotTeleop = TurtleBotTeleop()
	turtleBotTeleop.main()


