#!/usr/bin/env python3
import sys
from geometry_msgs.msg import Twist
import rospy
import threading
import matplotlib as plt
from matplotlib.figure import Figure
import matplotlib.pyplot as plt2
import numpy as np
import time

# Universidad de los Andes
# Robotica 2020 - 20
# Taller 1
# Solucion del segundo punto Taller 1 Robotica 2020 - 20

class TurtleBotGraph():

	def __init__(self):
		self.x = []
		self.y = []
		self.bandera = False

	def positionCallback(self, data):
		self.x.append(data.linear.x)
		self.y.append(data.linear.y)
		self.bandera = True

	def graficar(self):
		fig = plt2.figure()
		ax = fig.add_subplot(111)
		while True:
			if self.bandera:
				plt2.ion()
				line = ax.plot(self.x,self.y)
				plt2.show()
				plt2.pause(0.1)
				plt2.savefig("trayectoria_punto2.png")

	def main(self):
		rospy.init_node('turtle_bot_graph', anonymous = False)
		rospy.Subscriber('/turtlebot_position', Twist , self.positionCallback)
		grap= threading.Thread(target=self.graficar)
		grap.start() 
		rospy.spin()

if __name__ == '__main__':
	turtleBotGraph = TurtleBotGraph()
	turtleBotGraph.main()


